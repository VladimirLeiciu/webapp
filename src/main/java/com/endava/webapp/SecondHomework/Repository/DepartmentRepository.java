package com.endava.webapp.SecondHomework.Repository;

import com.endava.webapp.SecondHomework.Entity.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
public class DepartmentRepository {
    @Autowired
    EntityManager entityManager;

    public Department getDepartment() {
        return entityManager.find(Department.class, 10L);
    }
}
