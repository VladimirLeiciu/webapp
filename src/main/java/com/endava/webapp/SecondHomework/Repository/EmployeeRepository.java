package com.endava.webapp.SecondHomework.Repository;

import com.endava.webapp.SecondHomework.Entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
public class EmployeeRepository {

    @Autowired
    private EntityManager entityManager;

    public Employee getEmployee() {
        return entityManager.find(Employee.class, 101L);
    }
}
