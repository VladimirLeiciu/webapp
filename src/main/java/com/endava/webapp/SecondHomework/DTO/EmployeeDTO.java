package com.endava.webapp.SecondHomework.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeDTO {
    private Long employee_id;
    private String first_name;
    private String last_name;
}
