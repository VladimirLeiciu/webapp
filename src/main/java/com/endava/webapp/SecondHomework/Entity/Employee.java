package com.endava.webapp.SecondHomework.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GeneratorType;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.sql.Date;

@Data
@NoArgsConstructor
@Entity
@Table(name = "employees")
public class Employee {

    @Id
    @Column(name = "employee_id")
    private Long employee_id;

    @Column(nullable = false)
    @NotBlank
    @NotEmpty
    private String first_name;

    @Column(nullable = false)
    @NotBlank
    @NotEmpty
    private String last_name;

    @Column
    @Pattern(regexp = "^.+@.+$")
    private String email;

    @Column
    @Pattern(regexp = "^0[0-9]{8}$")
    private String phone_number;

    @Column
    private Date hire_date;

    @Column
    @Min(value = 1)
    private Long salary;

    @Column
    private Double commission_pct;

    @ManyToOne(cascade = {CascadeType.DETACH, CascadeType.PERSIST})
    @JoinColumn(name = "employee_id",referencedColumnName = "employee_id", insertable = false, updatable = false)
    private Employee manager_id;
}
