package com.endava.webapp.SecondHomework.Entity;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@Entity
@Table(name = "departments")
public class Department {

    @Id
    private Long department_id;

    @Column
    @NotNull
    @NotEmpty
    @NotBlank
    private String department_name;

    @PrimaryKeyJoinColumn
    @OneToOne(cascade = {CascadeType.DETACH, CascadeType.PERSIST})
    private Employee manager_id;
}
