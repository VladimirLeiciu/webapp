package com.endava.webapp.SecondHomework.Controllers;

import com.endava.webapp.SecondHomework.DTO.EmployeeDTO;
import com.endava.webapp.SecondHomework.Entity.Department;
import com.endava.webapp.SecondHomework.Entity.Employee;
import com.endava.webapp.SecondHomework.Repository.DepartmentRepository;
import com.endava.webapp.SecondHomework.Repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api")
public class EmployeesController {

    @Autowired
    EmployeeRepository repository;

    @Autowired
    DepartmentRepository departmentRepository;

    @GetMapping(path = "/getemp")
    public Employee getEmployee() {
        Employee employee = repository.getEmployee();
        EmployeeDTO employeeDTO = new EmployeeDTO(employee.getEmployee_id(), employee.getFirst_name(), employee.getLast_name());
        return employee;
    }

    @GetMapping(path = "/getdep")
    public Department getDep() {
        return departmentRepository.getDepartment();
    }
}
